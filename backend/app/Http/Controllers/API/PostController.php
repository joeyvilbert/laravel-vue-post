<?php

namespace App\Http\Controllers\API;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class PostController extends Controller
{
    public function create(Request $request)
    {

        $fields = $request->validate([
            'title' => 'string',
            'content' => 'string'
        ]);

        $Post = Post::create($fields);
        return [
            'code' => 200,
            'message' => 'success',
            'data' => $fields
        ];
        //     return ApiHelper::success($Post);
        // } catch (Exception $th) {
        //     return ApiHelper::error($th);
        // }
    }

    public function read($id)
    {
        $Post = Post::findOrFail($id);
        return [
            'code' => 200,
            'message' => 'success',
            'data' => $Post
        ];
    }

    public function readAll()
    {
        $Posts = Post::all();
        return [
            'code' => 200,
            'message' => 'success',
            'data' => $Posts
        ];
    }

    public function update(Request $request, $id)
    {
        $Post = Post::findOrFail($id);
        $Post->update($request->all());
        return [
            'code' => 200,
            'message' => 'success',
            'data' => $Post
        ];
    }

    public function delete($id)
    {
        $Post = Post::destroy($id);
        return [
            'code' => 200,
            'message' => 'success',
            'data' => $Post
        ];
    }
}
