import { createWebHistory, createRouter } from "vue-router";
import ListContact from "../components/ListPost.vue";

const routes = [{ path: "/", name: "Home", component: ListContact }];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;